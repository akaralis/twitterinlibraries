/*
 * This TEST checks if bing-spelling-suggestions module works correctly
 */

var bingSpellingSuggestions = require('../bing-spelling-suggestions');

exports['test'] = function (test) {
    var expectedResults = [ //Last updated: 22/5/2016
        'cloud computing'
    ];

    var i;
    var ok = true;
    var results = bingSpellingSuggestions.getSpellingSuggestions("cloudComputing", function (error, results) {
        for (i = 0; i < expectedResults.length; i++) {
            if (results.indexOf(expectedResults[i]) == -1) {
                ok = false;
                break;
            }
        }
    });

    test.equal(ok, true);
    test.done();

};

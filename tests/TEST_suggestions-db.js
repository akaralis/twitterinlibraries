/*
 * This TEST checks if suggestions-db module works correctly
 */

var suggestionsDB = require('../suggestions-db');
var suggestionSamples = [];
var i;
var availKeywordTypes = [['author'], ['subject'], ['title'], ['author', 'subject']];

//create samples
for (i = 0; i < 10; i++) {
    suggestionSamples[i] = {
        keyword: "keyword" + (i + 1),
        keyword_types: availKeywordTypes[i % 4],
        url: "url" + (i + 1),
        hashtag: "hashtag" + (i + 1),
		counter: 0
    }
}

exports['test add'] = function (test) {
    var i;
    var ok = true;
    var results = suggestionsDB.addSuggestions(suggestionSamples, function (error, results) {

        if (error) {
            ok = false;
            test.fail();
            test.done();
            suggestionsDB.release();
        } else {
            results = suggestionsDB.getSuggestions(function (error, results) {
                if (error) {
                    ok = false;
                } else {
                    results.sort(function (a, b) {
                        var aNumber = parseInt(a.keyword.substring(7));
                        var bNumber = parseInt(b.keyword.substring(7));

                        return aNumber - bNumber;
                    });

                    for (i = 0; i < suggestionSamples.length; i++) {
                        delete suggestionSamples[i]._id;
                        if (JSON.stringify(suggestionSamples[i]) != JSON.stringify(results[i])) {
                            ok = false;
                            break;
                        }
                    }
                }

                test.equal(ok, true);
                test.done();

                suggestionsDB.release();
            });
        }
    });
};



/*
 * This TEST checks if twitter-apponly-oauth module works correctly
 */

var twitterSearchAPI = require('../twitter-search');

exports['test'] = function (test) {
    var expectedResults = [ //Last updated: 22/5/2016
        'cloud computing'
    ];

    var i;
    var ok = true;
    twitterSearchAPI.search({q: 'java'}, function (error, results) {
        if (!results || !results.statuses) {
            ok = false;
        }

        test.equal(ok, true);
        test.done();
    });


};

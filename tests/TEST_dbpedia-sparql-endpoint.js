/*
 * This TEST checks if dbpedia-sparql-endpoint module has an <<unrecognized character problem>> in a specified sparql query
 */

var dbpediaSparqlEndpoint = require('../dbpedia-sparql-endpoint');

for (var i = 1; i <= 1000; i++) {
    exports['test' + i] = function (test) {
        dbpediaSparqlEndpoint.sparqlQuery('select distinct ?Concept where {[] a ?Concept' + i + '} LIMIT 100', function (error, results) {

            test.equal(error, null);
            test.done();

        });
    }
}

dbpediaSparqlEndpoint.release();
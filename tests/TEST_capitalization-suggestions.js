/*
 * This TEST checks if capitalization-suggestions module works correctly
 */

var capitalizationSuggestionsEndpoint = require('../capitalization-suggestions');

exports['test'] = function (test) {
    var expectedResults = [
        'IEEE Cloud Computing 2015',
        'iEEE Cloud Computing 2015',
        'IEEE cloud Computing 2015',
        'iEEE cloud Computing 2015',
        'IEEE Cloud computing 2015',
        'iEEE Cloud computing 2015',
        'IEEE cloud computing 2015',
        'iEEE cloud computing 2015'
    ];

    var i;
    var ok = true;
    var results = capitalizationSuggestionsEndpoint.getCapitalizationSuggestions("IEEE cloud computing 2015");

    for (i = 0; i < expectedResults.length; i++) {
        if (results.indexOf(expectedResults[i]) == -1) {
            ok = false;
            break;
        }
    }

    test.equal(ok, true);
    test.done();

};
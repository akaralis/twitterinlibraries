var ApplicationParameters = require("./ApplicationParameters");
var bearer_token = null;
var twitter_oauth_module;

/**
 * Implements the <<GET search/tweets>> call of Twitter REST API
 * @see https://dev.twitter.com/rest/reference/get/search/tweets
 * @param params see https://dev.twitter.com/rest/reference/get/search/tweets#api-param-search_query
 * @param callback the callback function
 */
function search(params, callback) {
    var paramKeys = Object.keys(params);
    var i;

    if (bearer_token == null) {
        twitter_oauth_module = require("./twitter-apponly-oauth");
        twitter_oauth_module.getBearerToken(
            ApplicationParameters.TwitterCredentials.CONSUMER_KEY,
            ApplicationParameters.TwitterCredentials.CONSUMER_SECRET,
            function (error, btoken) {
                if (error) {
                    callback(error);
                }
                else {
                    bearer_token = btoken;
                    search(params, callback);
                }
            });
    }
    else {
        var https = require("https");
        var options = {
            hostname: "api.twitter.com",
            port: 443,
            method: "GET",
            headers: {
                "Authorization": "Bearer " + bearer_token,
                "Connection": "close"
            }
        };

        options.path = "/1.1/search/tweets.json?";

        for (i = 0; i < paramKeys.length; i++) {
            options.path += paramKeys[i] + "=" + encodeURIComponent(params[paramKeys[i]]) + "&";
        }

        options.path = options.path.substring(0, options.path.length - 1);

        var request = https.request(options, function (response) {
            var responseBody = "";
            if (response.statusCode != 200) {
                callback(new Error("The status code of the response is not 200(OK). " +
                    "[StatusCode:" + response.statusCode + "]"));
            }
            else {
                response.on("data", function (chunk) {
                    responseBody += chunk;
                });

                response.on("end", function () {
                    try {
                        callback(null, JSON.parse(responseBody));
                    }
                    catch (error) {
                        callback(error);
                    }
                });
            }
        });

        request.on("error", function (error) {
            callback(error);
        });

        request.end();
    }
}

module.exports.search = search;
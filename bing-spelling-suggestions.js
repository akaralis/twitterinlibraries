var ApplicationParameters = require("./ApplicationParameters");
var https = require('https');

/**
 * Makes spelling suggestions for the specified phrase.
 * The suggestions are returned as an array of strings
 * @param phrase the phrase for which suggestions will be searched
 * @param limit the (max) number of tweets to return. The default value is 1
 * @param callback the callback function
 */
function getSpellingSuggestions(phrase, callback, limit) {
    var options = {
        hostname: "api.cognitive.microsoft.com",
        port: 443,
        path: "/bing/v5.0/spellcheck/?text=" +
        encodeURIComponent(phrase) + "&$top=" +
        (!limit ? 1 : limit),
        method: "GET",
        headers: {
            "Ocp-Apim-Subscription-Key": ApplicationParameters.MSCognitiveCredentials.SUBSCRIPTION_KEY
        }
    };

    var request = https.request(options, function (response) {
        var response_body = "";
        if (response.statusCode != 200) {
            callback(new Error("bing. The status code of the response is not 200 (OK). " +
                "[StatusCode:" + response.statusCode + "]"));
        }
        else {
console.log("StatusCode:" + response.statusCode);
            response.on("data", function (chunk) {
                response_body += chunk;
            });
            response.on("end", function () {
                try {
                    var suggestions = [];                  
					if (JSON.parse(response_body).flaggedTokens[0]) { 
						var results = JSON.parse(response_body).flaggedTokens[0];
						var i;
						for (i = 0; i < results.suggestions.length; i++) {
							 suggestions[i] = results.suggestions[i].suggestion;
						}
                    }
                    callback(null, suggestions);
                }
                catch (error) {
                    callback(error);
                }
            });
        }
    });

    request.on("error", function (error) {
        callback(error);
    });
    request.end();
}

module.exports.getSpellingSuggestions = getSpellingSuggestions;

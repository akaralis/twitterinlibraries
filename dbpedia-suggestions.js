var dbpediaSparqlEndpoint = require("./dbpedia-sparql-endpoint");
var capitalizationSuggestionsEndpoint = require("./capitalization-suggestions");

/**
 * This function makes thematic category suggestions related to a specified phrase.
 * The suggestions are provided as an array of strings.
 * @param phrase the keyword(s) for which suggestions will be made
 * @param callback the callback function
 * @param limit specify the (max) number of suggestions to return. The default value is 1000
 */
function getCategories(phrase, callback, limit) {
    var query;
    var capitalizationSuggestions = capitalizationSuggestionsEndpoint.getCapitalizationSuggestions(phrase, "_");
    var i, asyncSearchCanceled = false, numAsyncSearchesFinished = 0;
    var categories = [];

    if (!limit) {
        limit = 1000;
    }

    for (i = 0; i < capitalizationSuggestions.length; i++) {
        query =
            "prefix dc: <http://purl.org/dc/terms/>\n" +
            "select distinct ?category where {\n" +
            "<http://dbpedia.org/resource/" + capitalizationSuggestions[i] + "> dc:subject ?category .} LIMIT " + limit;

        dbpediaSparqlEndpoint.sparqlQuery(query, function (error, results) {
            var categoriesIndex = categories.length, resultsIndex = 0;
            numAsyncSearchesFinished++;

            if (!asyncSearchCanceled) {
                if (error) {
                    asyncSearchCanceled = true;
                    callback(error);
                }
                else {
                    while (resultsIndex < results.length) {
                        categories[categoriesIndex] =
                            results[resultsIndex].category.value.substring(
                                "http://dbpedia.org/resource/Category:".length
                            ).replace(/_/g, " ");

                        categoriesIndex++;
                        resultsIndex++;
                    }

                    if (numAsyncSearchesFinished == capitalizationSuggestions.length) {
                        callback(null, categories);
                    }
                }
            }
        });
    }
}

/**
 * This function makes term suggestions related to a specified phrase.
 * The suggestions are provided as an array of strings.
 * @param phrase the keyword(s) for which suggestions will be made
 * @param callback the callback function
 * @param limit specify the (max) number of suggestions to return
 */
function getRelatedTerms(phrase, callback, limit) {
    var query;
    var capitalizationSuggestions = capitalizationSuggestionsEndpoint.getCapitalizationSuggestions(phrase, "_");
    var i, asyncSearchCanceled = false, numAsyncSearchesFinished = 0;
    var relatedTerms = [];

    if (!limit) {
        limit = 1000;
    }

    for (i = 0; i < capitalizationSuggestions.length; i++) {
        query = "prefix dc: <http://purl.org/dc/terms/>\n" +
            "select distinct ?name WHERE { ?categories rdfs:label '" + capitalizationSuggestions[i] + "'@en .\n" +
            "?relatedterms dc:subject ?categories .\n" +
            "?relatedterms rdfs:label ?name .\n" +
            "FILTER(langMatches(lang(?name), 'EN'))} LIMIT " + limit;

        dbpediaSparqlEndpoint.sparqlQuery(query, function (error, results) {
            var relatedTermsIndex = relatedTerms.length, resultsIndex = 0;
            numAsyncSearchesFinished++;

            if (!asyncSearchCanceled) {
                if (error) {
                    asyncSearchCanceled = true;
                    callback(error);
                }
                else {
                    while (resultsIndex < results.length) {
                        relatedTerms[relatedTermsIndex] = results[resultsIndex].name.value;

                        relatedTermsIndex++;
                        resultsIndex++;
                    }

                    if (numAsyncSearchesFinished == capitalizationSuggestions.length) {
                        callback(null, relatedTerms);
                    }
                }
            }
        });
    }
}

module.exports.getCategories = getCategories;
module.exports.getRelatedTerms = getRelatedTerms;

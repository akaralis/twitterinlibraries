var ApplicationParameters = require("./ApplicationParameters");
var bing_ss = require("./bing-spelling-suggestions");
var capitalization_suggestions = require("./capitalization-suggestions");
var dbpedia_suggestions = require("./dbpedia-suggestions");
var library_queries = require("./library-queries");
var twitter_search = require("./twitter-search");
var search_target_en = "programming";
var suggestions_db = require("./suggestions-db");

function logError(error) {
    console.error(error.stack);
}
/*jpap returns a set with all the distinct hashtags of an array of mongodb records */
function getHashtagsOutOfObjects(objects) {
	var setOfHashtags = new Set();
	if (objects) {
		for (var i = 0; i < objects.length; i++) {
			if (!setOfHashtags.has(objects[i].hashtag.toLowerCase())) {
				setOfHashtags.add(objects[i].hashtag.toLowerCase());
			}
		}
	}
	return setOfHashtags;
}
/*jpap returns a set with all the distinct keywords of an array of mongodb records */
function getKeywordsOutOfObjects(objects) {
	var setOfKeywords = new Set();
	if (objects) {
		for (var i = 0; i < objects.length; i++) {
			if (!setOfKeywords.has(objects[i].keyword.toLowerCase())) {
				setOfKeywords.add(objects[i].keyword.toLowerCase());
			}
		}
	}
	return setOfKeywords;
}

function makeBookSuggestions() {
    var makeSuggestionsBasedOnThisTrend = function (trend) {
        var storeSuggestion = function (error, suggestion) {
            if (error) {
                logError(error);
            }
            else {
				suggestions_db.getMostRecentSuggestions(function (error, records) {
					if (error) {
						logError(error);
					}
					else {
						if (!getKeywordsOutOfObjects(records).has(suggestion.keyword.toLowerCase())) { //we add the suggestion only if it is diffrent from the most recent ones (in 'PERIOD' days) in mongodb
							if (suggestion.url != "") {
								suggestion.hashtag = trend;
								suggestion.timestamp = new Date();
								suggestion.counter = 0;
								suggestions_db.addSuggestions([suggestion], function (error) {
									if (error) {
										logError(error);
									}
								});
							}
						}
					}
				});
            }
        };

        var i;

        dbpedia_suggestions.getCategories(
            trend,
            function (error, categories) {
                if (error) {
                    logError(error);
                }
                else {
                    for (i = 0; i < categories.length; i++) {
//console.log("fromDbpediaGetCategories ("+ trend +")");
                        library_queries.searchForBooks(search_target_en + " " + categories[i], ["subject"], storeSuggestion);
                    }
                }
            });

        dbpedia_suggestions.getRelatedTerms(
            trend,
            function (error, related_terms) {

                if (error) {
                    logError(error);
                }
                else {
                    for (i = 0; i < related_terms.length; i++) {
//console.log("fromDbpediaGetRelatedTerms ("+ trend +")");
                        library_queries.searchForBooks(search_target_en + " " + related_terms[i], ["subject"], storeSuggestion);
                    }
                }
            });
//console.log("DirectlyFromTwitter ("+ trend +")");
        library_queries.searchForBooks(search_target_en + " " + trend, ["subject"], storeSuggestion);

    };

    var since_id = 0;
    twitter_search.search({
        q: search_target_en,
        count: 5,
        lang: "en",
        result_type: "mixed",
        "since_id": since_id
    }, function (error, response) {

        var i, j, tweet, hashtags;

        for (i = 0; i < response.statuses.length; i++) { 
            tweet = response.statuses[i];

            if (!tweet.entities) {
                continue;
            }

            hashtags = tweet.entities.hashtags;
            for (j = 0; j < hashtags.length; j++) {
				var hashtag = hashtags[j].text;
				suggestions_db.getMostRecentHashtags(function (error, records) {
					if (error) {
						logError(error);
					}
					else {
						if (!getHashtagsOutOfObjects(records).has(hashtag.toLowerCase())) { //we add the hashtag only if it is diffrent from the most recent ones (in 'PERIOD' days) in mongodb
							bing_ss.getSpellingSuggestions(hashtag, function (error, suggestions) {
								if (error) {
									makeSuggestionsBasedOnThisTrend(hashtag);
//						logError(error.message);
								}
								else if (suggestions.length != 0) {
									makeSuggestionsBasedOnThisTrend(suggestions[0]);
								}
								else {
									makeSuggestionsBasedOnThisTrend(hashtag);
								}
							});
						}
					}
				});				
            }
        }

        since_id = response.search_metadata.max_id_str;
    });
}

(function main() {
    require("child_process").fork("./server.js");
    makeBookSuggestions();
    setInterval(function () {
        makeBookSuggestions();
    }, ApplicationParameters.SUGGESTIONS_REFRESH_TIMEOUT);
})();

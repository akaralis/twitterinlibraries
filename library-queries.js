var ApplicationParameters = require("./ApplicationParameters.js");
var http = require('http');

var queue = [];

function processNextRequest() {
    var requestHandler;
    if (queue.length != 0) {
        requestHandler = queue[0];
        setTimeout(function () {
            requestHandler();
        }, 50);
    }
}


/*function executeSRURequest(encoded_searchpart, callback) {
 var options = {
 hostname: ApplicationParameters.KOHA_HOST,
 port: ApplicationParameters.KOHA_SRU_PORT,
 path: "/biblios?" + encoded_searchpart,
 method: "GET"
 };

 var request = http.request(options, function(response) {
 var response_body = "";

 if (response.statusCode != 200) {
 callback(new Error("The status code of the response is not 200(OK). " +
 "[StatusCode:" + response.statusCode + "]"));
 }
 else {
 response.on("data", function(chunk) {
 response_body += chunk;
 });

 response.on("end", function() {
 callback(null, response_body);
 });
 }
 });

 request.on("error", function(error) {
 callback(error);
 });
 request.end();
 }*/
function searchForBooks(keyword, keyword_types, callback) {

    queue.push(function () {

        var query_finish = function () {
            queue.shift();
            processNextRequest();
        };


        var i, z /*, encoded_keyword = encodeURIComponent(keyword).replace(new RegExp("%20", 'g'), "+")*/;
        var path /*, book_url_list*/;
        var search_canceled = false;
        var /*author_flag = false,*/
            subject_flag = false
        /*,
         title_flag = false*/
            ;
        var async_search_counter = keyword_types.length;
        var words = keyword.split(" ");
        var useful_keyword_types = [];

        var invalidKeywordTypeCombination = function () {
            query_finish();
            search_canceled = true;
            callback(new Error("Invalid keyword type combination"));
        };

        for (i = 0; i < keyword_types.length; i++) {
            path = "/xmlui/discover?";
            switch (keyword_types[i].toLowerCase()) {
                // case "author":
                //     if (author_flag) {
                //         invalidKeywordTypeCombination();
                //         return;
                //     }
                //     path += "filtertype_1=author&filter_relational_operator_1=equals&filter_1=" + encoded_keyword;
                //     author_flag = true;
                //     break;
                case "subject":
                    if (subject_flag) {
                        invalidKeywordTypeCombination();
                        return;
                    }

                    /////////////////////////////////////////////
                    path += "filtertype_1=subject&filter_relational_operator_1=contains&filter_1=" + encodeURIComponent(words[0]);

                    for (z = 1; z < words.length; z++) {
                        path += "&filtertype_" + (z + 1) + "=subject&filter_relational_operator_" + (z + 1) + "=contains&filter_" + (z + 1) + "=" + encodeURIComponent(words[z]);
                    }

                    ////////////////////////////////////////////

                    subject_flag = true;
                    break;
                // case "title":
                //     if (title_flag) {
                //         invalidKeywordTypeCombination();
                //         return;
                //     }
                //     path += "filtertype_1=title&filter_relational_operator_1=equals&filter_1=" + encoded_keyword;

                //     title_flag = true;
                //     break;
                default:
                    invalidKeywordTypeCombination();
                    return;
            }

            (function () {
                var keyword_type = keyword_types[i];
                var j,
                    options = {
                        hostname: ApplicationParameters.LIBRARY_HOST,
                        port: ApplicationParameters.LIBRARY_HTTP_PORT,
                        'path': path,
                        method: "GET"
                    };

                if (!search_canceled) {

                    var request = http.request(options, function (response) {
                        var response_body = "";

                        if (response.statusCode != 200) {
                            callback(new Error("The status code of the response is not 200(OK). " +
                                "[StatusCode:" + response.statusCode + "]"));
                        }
                        else {
                            response.on("data", function (chunk) {
                                response_body += chunk;
                            });

                            response.on("end", function () {
                                if (response_body.indexOf("Search produced no results") == -1 && response_body.indexOf("Η αναζήτηση δεν παρήγαγε αποτελέσματα") == -1) {
                                    useful_keyword_types.push(keyword_type);
                                }
                                if (--async_search_counter == 0) {
                                    query_finish();
                                    /*                                    if (useful_keyword_types.length > 0) {
                                     book_url_list = ApplicationParameters.LIBRARY_HOST + ":" + ApplicationParameters.LIBRARY_HTTP_PORT + "/xmlui/discover?";
                                     book_url_list += "filtertype_1=" + useful_keyword_types[0] + "&filter_relational_operator_1=equals&filter_1=" + encoded_keyword;

                                     }

                                     for (j = 1; j < useful_keyword_types.length; j++) {
                                     book_url_list += "&filtertype_" + (j + 1) + "=" + useful_keyword_types[j] + "&filter_relational_operator_" + (j + 1) + "=equals&filter_" + (j + 1) + "=" + encoded_keyword;
                                     }*/

                                    callback(null, {
                                        "keyword": keyword,
                                        "keyword_types": useful_keyword_types,
                                        "url": (useful_keyword_types.length == 0 ? "" : ApplicationParameters.LIBRARY_HOST + ":" + ApplicationParameters.LIBRARY_HTTP_PORT + path) //todo patch
                                    });
                                }
                            });
                        }
                    });

                    request.on("error", function (error) {
                        query_finish();
                        search_canceled = true;
                        callback(error);
                    });
                    request.end();
                }
            })();
        }
    });

    if (queue.length == 1) processNextRequest();

    /*  var sru_req_base_searchpart = "version=1.1&operation=searchRetrieve&query=";
     var book_list_url = "http://" + ApplicationParameters.KOHA_HOST +
     ":" + ApplicationParameters.KOHA_HTTP_PORT +
     "/cgi-bin/koha/opac-search.pl?";

     var sru_req_searchpart, book_list_url_suffix, i, encoded_keyword;
     var search_canceled = false;
     var author_flag = false,
     subject_flag = false,
     title_flag = false;
     var async_search_counter = keyword_types.length;
     var useful_keyword_types = [];

     var invalidKeywordTypeCombination = function() {
     search_canceled = true;
     callback(new Error("Invalid keyword type combination"));
     };

     for (i = 0; i < keyword_types.length; i++) {
     encoded_keyword = keyword;

     if (keyword.indexOf("\"") != 0 || keyword.lastIndexOf("\"") != keyword.length - 1) {
     encoded_keyword = "\"" + keyword.split(" ").join("\" \"") + "\"";
     }

     encoded_keyword = encodeURIComponent(encoded_keyword);

     switch (keyword_types[i].toLowerCase()) {
     case "author":
     if (author_flag) {
     invalidKeywordTypeCombination();
     return;
     }
     sru_req_searchpart = sru_req_base_searchpart + "Author%3D" + encoded_keyword;
     book_list_url_suffix = "idx=au&q=" + encodeURIComponent(keyword);
     author_flag = true;
     break;
     case "subject":
     if (subject_flag) {
     invalidKeywordTypeCombination();
     return;
     }
     sru_req_searchpart = sru_req_base_searchpart + "Subject%3D" + encoded_keyword;
     book_list_url_suffix = "idx=su&q=" + encodeURIComponent(keyword);
     subject_flag = true;
     break;
     case "title":
     if (title_flag) {
     invalidKeywordTypeCombination();
     return;
     }
     sru_req_searchpart = sru_req_base_searchpart + "Title%3D" + encoded_keyword;
     book_list_url_suffix = "idx=ti&q=" + encodeURIComponent(keyword);
     title_flag = true;
     break;
     default:
     invalidKeywordTypeCombination();
     return;
     }

     (function() {
     var keyword_type = keyword_types[i];
     var book_list_url_suffix_ = book_list_url_suffix;
     var start_tag, end_tag, number_of_records;
     executeSRURequest(sru_req_searchpart, function(error, sru_response) {
     if (!search_canceled) {

     if (error) {
     search_canceled = true;
     callback(error);
     }
     else {
     start_tag = "<zs:numberOfRecords>";
     end_tag = "</zs:numberOfRecords>";
     number_of_records = parseInt(sru_response.substring(sru_response.indexOf(start_tag) + start_tag.length, sru_response.indexOf(end_tag)));

     if (isNaN(number_of_records)) {
     search_canceled = true;
     callback(new Error("Unexpected SRU Response"));
     }
     else {
     if (number_of_records > 0) {

     if (useful_keyword_types.length > 0) {
     book_list_url = book_list_url + "&op=or&" + book_list_url_suffix_;
     }
     else {
     book_list_url = book_list_url + book_list_url_suffix_;
     }

     useful_keyword_types.push(keyword_type);
     }

     if (--async_search_counter == 0) {
     callback(null, {
     "keyword": keyword,
     "keyword_types": useful_keyword_types,
     "url": (useful_keyword_types.length == 0 ? "" : book_list_url)
     });
     }
     }
     }
     }
     });
     })();
     }*/
}


exports.searchForBooks = searchForBooks;
// searchForBooks("Bασιλείου, Νικόλαος", ['subject'], function(error, res) {
//     var i;
//     console.log(error)
//     console.log(res)

// });

/*
 searchForBooks("Programming languages with an ISO standard", ['subject'], function (error, res) {
 console.log(res)
 });*/

var ApplicationParameters = require("./ApplicationParameters.js");
var suggestions_db = require("./suggestions-db.js");
var http = require("http");
var HttpDispatcher = require('httpdispatcher');
var dispatcher = new HttpDispatcher();

process.on("disconnect", function () {
    process.exit(0);
});

function gup( href, name ) {
  name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  var regexS = "[\\?&]"+name+"=([^&#]*)";
  var regex = new RegExp( regexS );
  var results = regex.exec( href );
  if( results == null )
    return "";
  else
    return results[1];
}

function handleRequest(request, response) {
	 suggestions_db.getSuggestions(function (error, suggestions) {
		if (error) {
			response.writeHead(500);
		}
		else {
			response.writeHead(200, {
				"Content-Type": "application/json",
				"Access-Control-Allow-Origin": "*",
				"Cache-Control": "no-cache, no-store, must-revalidate",
				"Pragma": "no-cache",
				"Expires": "0",
			});
			response.write(JSON.stringify({
				"suggestions": suggestions
			}));
		}
		response.end();
	}, ApplicationParameters.SUGGESTIONS_LIMIT);
}

dispatcher.onGet("/", handleRequest);
dispatcher.onPost("/", handleRequest);
dispatcher.onHead("/", function(request, response) {
	response.writeHead(200);
	response.end();
});

dispatcher.onGet("/discover", function(request, response) {
	var id = gup(request.url, 'id');
	suggestions_db.addCounterSuggestion(id, function (error, affected) {
		if (error) {
			response.writeHead(500);
		}
		response.end();
	});
	var library = 'http://' + ApplicationParameters.LIBRARY_HOST + ':' + ApplicationParameters.LIBRARY_HTTP_PORT + '/xmlui' + request.url;
	response.writeHead(302,  {Location: library}); //should have been 307, but backwards compatibility is more important
	response.end();
});

http.createServer(function (request, response) {
	dispatcher.dispatch(request, response);

}).listen(ApplicationParameters.SERVER_PORT, ApplicationParameters.SERVER_HOST);
var ApplicationParameters = require("./ApplicationParameters.js");
var mongo = require('mongodb');
var mongoClient = require('mongodb').MongoClient;
var mongodbConnectionURL = "mongodb://" +
    ApplicationParameters.MONGODBCredentials.username + ":" +
    ApplicationParameters.MONGODBCredentials.password + "@" +
    ApplicationParameters.MONGODB_HOST + ":" +
    ApplicationParameters.MONGODB_PORT + "/" +
    ApplicationParameters.MONGODB_SCHEMA + "?autoReconnect=true";

var connectionOptions = {
    "db": {
        bufferMaxEntries: 0
    }
};

var db = null;
var lastdays = getLastdays(ApplicationParameters.PERIOD);
var lastdaysMonth = lastdays.getMonth() + 1;
var lastdaysDay = lastdays.getDate();
var lastdaysYear = lastdays.getFullYear();
var lastdaysDisplay = lastdaysYear + "/" + lastdaysMonth + "/" + lastdaysDay;	
/**
 * Stores the specified suggestions to the database.
 * Each suggestion should be an object with the following fields:
 * keyword: the keyword (set of words) that has been produced as a result of hashtag processing
 * keyword_types: author, title or subject
 * url: the url of the list of books that have been found after the search for books related to the specified keyword
 * hashtag: the twitter hashtag from which the keyword has been produced
 * @param {Array} suggestions the suggestions to be stored
 * @param callback the callback function
 */
function addSuggestions(suggestions, callback) {
    var collection;
    if (!db) {
        mongoClient.connect(mongodbConnectionURL, connectionOptions,
            function (error, database) {
                if (error) {
                    callback(error);
                }
                else {
                    db = database;
                    addSuggestions(suggestions, callback);
                }
            });
    }
    else {
        collection = db.collection("AssetSuggestions");
        collection.insertMany(suggestions, function (error, result) {
            callback(error);
        });
    }
}

/**
 * Returns the 'limit' most recently suggestions
 * @param callback the callaback function
 * @param limit specify the (max) number of suggestions to return. The default value is 7
 */
function getSuggestions(callback, limit) {
    var collection;
    if (!limit) {
        limit = 7;
    }

    if (!db) {
        mongoClient.connect(mongodbConnectionURL, connectionOptions,
            function (error, database) {
                if (error) {
                    callback(error);
                }
                else {
                    db = database;
                    getSuggestions(callback, limit);
                }
            });
    }
    else {	
        collection = db.collection("AssetSuggestions");
        collection.aggregate([{ 
			$match : { _id: {$gt: objectIdWithTimestamp(lastdaysDisplay)} }
        }, {
			$group: {
                _id: { keyword: "$keyword" },
				counter : { $sum : "$counter" },
                doc: {
                    $first: {
                        id: "$_id",
						keyword: "$keyword",
                        keyword_types: "$keyword_types",
                        url: "$url",
                        hashtag: "$hashtag",
						timestamp: "$timestamp"
                    }
                }
            }
		}, {
            $project: {
                _id: 0,
                "id": "$doc.id",
                "keyword_types": "$doc.keyword_types",
                "url": "$doc.url",
				"keyword": "$doc.keyword",
                "hashtag": "$doc.hashtag",
				"timestamp": "$doc.timestamp",
				"counter": "$counter"
            }
		}, {
			$limit: limit
		}, {
			$sort: { "counter": -1, "id": -1 }
        }], function (error, documents) {
            if (error) {
                callback(error);
            }
            else {
                callback(null, documents);
            }
        });
    }
}

/*jpap get the most recent (for 'PERIOD' days) hashtags 
The function returns an Array of just the 'hashtag' of the mongodb record */
function getMostRecentHashtags(callback) {
    var collection;
    if (!db) {
        mongoClient.connect(mongodbConnectionURL, connectionOptions,
            function (error, database) {
                if (error) {
                    callback(error);
                }
                else {
                    db = database;
                    getMostRecentHashtags(callback);
                }
            });
    }
    else {
		collection = db.collection("AssetSuggestions");
		collection.find({_id: {$gt: objectIdWithTimestamp(lastdaysDisplay)}}, {_id: 0, hashtag: 1}).toArray(function(error, result) {
			if (error) {
                callback(error);
            }
            else {
				callback(null, result);
            }			
        }); 
    }
}
/*jpap get the most recent (for 'PERIOD' days) suggestions 
The function returns an Array of just the 'keyword' of the mongodb record */
function getMostRecentSuggestions(callback) {
    var collection;
    if (!db) {
        mongoClient.connect(mongodbConnectionURL, connectionOptions,
            function (error, database) {
                if (error) {
                    callback(error);
                }
                else {
                    db = database;
                    getMostRecentSuggestions(callback);
                }
            });
    }
    else {
		collection = db.collection("AssetSuggestions");
		collection.find({_id: {$gt: objectIdWithTimestamp(lastdaysDisplay)}}, {_id: 0, keyword: 1}).toArray(function(error, result) {
			if (error) {
                callback(error);
            }
            else {
				callback(null, result);
            }			
        }); 
    }
}
/**
 * Returns the number of Documents whoose counter has been increased
 * @param callback the callaback function
 */
function addCounterSuggestion(id, callback) {
    var collection;
    if (!db) {
        mongoClient.connect(mongodbConnectionURL, connectionOptions,
            function (error, database) {
                if (error) {
                    callback(error);
                }
                else {
                    db = database;
                    addCounterSuggestion(id, callback);
                }
            });
    }
    else {
        collection = db.collection("AssetSuggestions");
		var oid = new mongo.ObjectID(id);
		collection.update({_id: oid}, { $inc: { "counter": 1 } }, function (error, WriteResult) {
			if (error) {
				callback(error);
			} else {
				callback(null, WriteResult);
			}
		});			
    }
}

// This function returns an ObjectId embedded with a given datetime
// Accepts both Date object and string input
// should be used:  Find all documents created after midnight on May 25th, 1980
// db.mycollection.find({ _id: { $gt: objectIdWithTimestamp('1980/05/25') } });
function objectIdWithTimestamp(timestamp) {
    // Convert string date to Date object (otherwise assume timestamp is a date)
    if (typeof(timestamp) == 'string') {
        timestamp = new Date(timestamp);
    }
    // Convert date object to hex seconds since Unix epoch
    var hexSeconds = Math.floor(timestamp/1000).toString(16);
    // Create an ObjectId with that hex timestamp
    var constructedObjectId = mongo.ObjectId(hexSeconds + "0000000000000000");
    return constructedObjectId
}

// This function gets the date 'days' ago
function getLastdays(days){
    var today = new Date();
    var lastdays = new Date(today.getFullYear(), today.getMonth(), today.getDate() - days);
    return lastdays;
}

/**
 * Aborts any pending requests and closes any open connections.
 */
function release() {
    db.close();
}

module.exports.addSuggestions = addSuggestions;
module.exports.getSuggestions = getSuggestions;
module.exports.addCounterSuggestion = addCounterSuggestion;
module.exports.getMostRecentSuggestions = getMostRecentSuggestions;
module.exports.getMostRecentHashtags = getMostRecentHashtags;
module.exports.release = release;

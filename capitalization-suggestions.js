/**
 * Creates the transformations of a phrase that result from considering either the upper or lower case of the first letter of
 * each word which the phrase is consisted of
 * @param objectphrase: {suggestion: phrase} the phrase for which suggestions will be made
 * @param separator a sequence of characters that will replace the space between the words. if it is not specified,
 * the space character is used
 * @returns {Array} the suggestions
 */
function getCapitalizationSuggestions(phrase, separator) {
    var tokens = phrase.split(" ");
    var results = [];
    var firstCharacter, i, j, resSize;

    if (!separator) {
        separator = " ";
    }

    if (tokens.length != 0) {
        firstCharacter = tokens[0].charAt(0);

        if (isNaN(parseInt(firstCharacter))) {
            results.push(firstCharacter.toUpperCase() + tokens[0].substring(1));
            results.push(firstCharacter.toLowerCase() + tokens[0].substring(1));
        }
        else {
            results.push(tokens[0]);
        }

        for (i = 1; i < tokens.length; i++) {
            resSize = results.length;
            firstCharacter = tokens[i].charAt(0);

            for (j = 0; j < resSize; j++) {

                if (isNaN(parseInt(firstCharacter))) {
                    results.push(results[j] + separator + firstCharacter.toLowerCase() + tokens[i].substring(1));
                    results[j] = results[j] + separator + firstCharacter.toUpperCase() + tokens[i].substring(1);
                }
                else {
                    results[j] = results[j] + separator + tokens[i];
                }
            }
        }
    }

    return results;
}

module.exports.getCapitalizationSuggestions = getCapitalizationSuggestions;

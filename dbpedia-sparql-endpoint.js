var ApplicationParameters = require("./ApplicationParameters.js");
var http = require("http");
var queue = [];
var lastRequest = null;
var requestsPerSecond = 1000;
var options = {
    hostname: "dbpedia.org",
    port: 80,
    agent: new http.Agent({keepAlive: true, maxSockets: 4, maxFreeSockets: 4}),
    method: "GET"
}

/**
 * The following two functions provide a mechanism for controlling the rate at which sparql queries are sent at
 * dbpedia's sparql endpoint.
 */
function processNextRequest() {
    var requestHandler;
    if (queue.length != 0) {
        requestHandler = queue[0];
        setTimeout(function () {
            requestHandler();
        }, requestsPerSecond == -1 ? 0 : (1 / requestsPerSecond) * 1000);
    }
}


function queryFinish() {
    queue.shift();
    processNextRequest();
};

/**
 * A function for the execution of sparql queries in dbpedia's sparql endpoint.
 * The results are provided as an array of objects.
 * These objects have the same syntax as the json results format of dbpedia's sparql endpoint
 * @param query the query to be executed
 * @param callback the callback function
 */
function sparqlQuery(query, callback) {

    queue.push(function () {

        options.path = "/sparql?default-graph-uri=http%3A%2F%2Fdbpedia.org&query=" + encodeURI(query) +
            "&format=application%2Fsparql-results%2Bjson&timeout=" + ApplicationParameters.DBPEDIA_TIMEOUT;

        var request = http.request(options, function (response) {
            var response_body = "";

            if (response.statusCode != 200) {
                queryFinish();
                callback(new Error("The status code of the response is not 200(OK). " +
                    "[StatusCode:" + response.statusCode + "]" + " " + query));
            }
            else {
                response.on("data", function (chunk) {
                    response_body += chunk;
                });

                response.on("end", function () {
                    queryFinish();
                    try {
                        var json_results = JSON.parse(response_body).results.bindings;
                        callback(null, json_results);
                    }
                    catch (error) {
                        callback(error);
                    }
                });
            }
        });

        request.on("error", function (error) {
            queryFinish();
            callback(error);
        });
        request.end();

        lastRequest = request;
    });

    if (queue.length == 1) processNextRequest();
}

/*
 * Aborts any pending requests and closes any open 'keep-alive' connections.
 */
function release() {
    queue = [];
    if (lastRequest) lastRequest.abort();
}

module.exports.sparqlQuery = sparqlQuery;
module.exports.release = release;
module.exports.requestsPerSecond = requestsPerSecond; //for unlimited rate set the value of this parameter to -1
/**
 * Provides Application-only authentication for the clients of Twitter API.
 * @see https://dev.twitter.com/oauth/application-only
 * @param consumerKey the consumer key
 * @param consumerSecret the consumer secret
 * @param callback the callback function
 */
function getBearerToken(consumerKey, consumerSecret, callback) {

    var bearerTokenCredentials =
        new Buffer(encodeURIComponent(consumerKey) + ":" +
            encodeURIComponent(consumerSecret)).toString("base64");

    var https = require("https");
    var options = {
        hostname: "api.twitter.com",
        port: 443,
        path: "/oauth2/token",
        method: "POST",
        headers: {
            "Authorization": "Basic " + bearerTokenCredentials,
            "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
            "Connection": "close"
        }
    };

    var request = https.request(options, function (response) {
        var responseBody = "";

        if (response.statusCode != 200) {
            callback(new Error("The status code of the response is not 200(OK). " +
                "[StatusCode:" + response.statusCode + "]"));
        }
        else {
            response.on("data", function (chunk) {
                responseBody += chunk;
            });

            response.on("end", function () {
                try {
                    var token_type = JSON.parse(responseBody).token_type;
                    if (token_type != "bearer") {
                        throw new Error("Token type is not set to bearer. " +
                            "[TokenType:" + token_type + "]");
                    }

                    var bearer_token = JSON.parse(responseBody).access_token;
                    callback(null, bearer_token);
                }
                catch (error) {
                    callback(error);
                }
            });
        }
    });

    request.on("error", function (error) {
        callback(error);
    });

    request.write("grant_type=client_credentials");
    request.end();
}

module.exports.getBearerToken = getBearerToken;
